from qgis.utils import iface
from qgis.core import QgsProject, QgsVectorFileWriter, QgsCoordinateReferenceSystem

layer = QgsProject.instance().mapLayer("[% @layer_id %]")

layer.startEditing()

if layer.selectedFeatureCount()>1:
    features = layer.selectedFeatures()
    geom = None
    for feat in features:
        if geom == None:
           geom = feat.geometry()
        else:
           geom = geom.combine(feat.geometry())
    new_feat = QgsFeature()
    new_feat.setGeometry(geom)
    new_feat.setFields(layer.fields())

    layer.addFeature(new_feat)

    success = iface.openFeatureForm(layer, new_feat, True)

    if success: 
        iface.messageBar().pushSuccess("qForst", "Flächen erfolgreich verbunden")
        layer.commitChanges()
    else:
        iface.messageBar().pushWarning("qForst", "Fehler: Da ist was schief gegangen...")

else:
    iface.messageBar().pushWarning("qForst", "Fehler: Es müssen mind. zwei Flächen gewählt sein!")
