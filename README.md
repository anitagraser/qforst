# qForst 
Forsteinrichtungsprogramm auf QGIS-Basis 
Version: 211206 für QGS 3.22+

# Bestandteile

- Vorlage-Projekt (qForst.qgz)
- Vorlage-Datenbank (DB.gpkg)
  - bestand
  - wege
  - betrieb
- QGIS-Nutzerprofil (material/profiles/qForstUse)
- Open Sans Schriftart (OpenSans.zip) (ab QGIS 3.28 nicht mehr erforderlich)

# Sprache
in qForst, QGIS und hier in Deutsch, da die Anwender*Innen Deutsch bevorzugen
